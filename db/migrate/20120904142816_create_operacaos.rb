class CreateOperacaos < ActiveRecord::Migration
  def change
    create_table :operacaos do |t|
      t.string :tipo

      t.timestamps
    end
  end
end
