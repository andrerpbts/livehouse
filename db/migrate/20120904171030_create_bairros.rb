class CreateBairros < ActiveRecord::Migration
  def change
    create_table :bairros do |t|
      t.string :nome
      t.references :cidade

      t.timestamps
    end
    add_index :bairros, :cidade_id
  end
end
