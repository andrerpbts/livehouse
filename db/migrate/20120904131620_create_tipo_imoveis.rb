class CreateTipoImoveis < ActiveRecord::Migration
  def change
    create_table :tipo_imoveis do |t|
      t.string :tipo_imovel

      t.timestamps
    end
  end
end
