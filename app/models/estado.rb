class Estado < ActiveRecord::Base
  attr_accessible :nome, :sigla
  has_many :cidades
  validates :nome, :presence => true
  validates :sigla, :presence => true
  
  def to_s
    "#{nome}"
  end
end
