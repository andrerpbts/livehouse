class Bairro < ActiveRecord::Base
  belongs_to :cidade
  attr_accessible :nome, :cidade_id
  
  validates :nome, :cidade_id, :presence => true

  def to_s
    "#{nome}"
  end
end
