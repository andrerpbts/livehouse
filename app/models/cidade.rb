class Cidade < ActiveRecord::Base
  belongs_to :estado

  attr_accessible :nome, :estado_id

  has_many :bairros

  validates :nome, :estado_id, :presence => true

  def to_s
    "#{nome}"
  end
end
